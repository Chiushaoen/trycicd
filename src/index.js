import React from 'react';
import ReactDOM from 'react-dom/client';
import GuessGameComponent from './components/GuessGameComponent';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<GuessGameComponent expected='1234' />);
